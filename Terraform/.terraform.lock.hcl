# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/null" {
  version = "3.2.2"
  hashes = [
    "h1:IMVAUHKoydFrlPrl9OzasDnw/8ntZFerCC9iXw1rXQY=",
  ]
}

provider "registry.terraform.io/yandex-cloud/yandex" {
  version = "0.104.0"
  hashes = [
    "h1:pM1/J/bDuQdqibc2iHy8pLJEZCP8kqjSA5qniZu+Gss=",
  ]
}
