data "yandex_compute_image" "my-ubuntu-2204-1" {
  family = "ubuntu-2204-lts"
}

resource "yandex_compute_instance" "agent-vm-1" {
  name        = "agent-vm-1"
  platform_id = "standard-v1"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = "${data.yandex_compute_image.my-ubuntu-2204-1.id}"
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.my-sn-1.id
    nat = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/cloud_vm.pub")}"
  }
}

resource "null_resource" "copy_key" {
  provisioner "file" {
    source      = "~/.ssh/cloud_vm"
    destination = "/home/ubuntu/.ssh/cloud_vm"

    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = file("~/.ssh/cloud_vm")
      host        = yandex_compute_instance.agent-vm-1.network_interface.0.nat_ip_address
    }
  }
}

resource "yandex_compute_instance" "server-vm-1" {
  name        = "server-vm-1"
  platform_id = "standard-v1"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = "${data.yandex_compute_image.my-ubuntu-2204-1.id}"
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.my-sn-1.id
    nat = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/cloud_vm.pub")}"
  }
}

resource "yandex_vpc_network" "my-net-1" {
  name = "my-net-1"
}
resource "yandex_vpc_subnet" "my-sn-1" {
  network_id     = yandex_vpc_network.my-net-1.id
  v4_cidr_blocks = ["192.168.1.0/24"]
}


resource "yandex_dns_zone" "main_zone" {
  name        = "my-public-zone"
  description = "A-zone_devopstestinglabs.ru"

  labels = {
    label1 = "label-1-value"
  }

  zone             = "devopstestinglabs.ru."
  public           = true
  private_networks = [yandex_vpc_network.my-net-1.id]
}

resource "yandex_dns_recordset" "rs1" {
  zone_id = yandex_dns_zone.main_zone.id
  name    = "agent.devopstestinglabs.ru."
  type    = "A"
  ttl     = 200
  data    = [(yandex_compute_instance.agent-vm-1.network_interface.0.nat_ip_address)]
}

resource "yandex_dns_recordset" "rs2" {
  zone_id = yandex_dns_zone.main_zone.id
  name    = "server.devopstestinglabs.ru."
  type    = "A"
  ttl     = 200
  data    = [(yandex_compute_instance.server-vm-1.network_interface.0.nat_ip_address)]
}


output "external_ip_agent-vm-1" {
  value = yandex_compute_instance.agent-vm-1.network_interface.0.nat_ip_address
}

output "external_ip_server-vm-1" {
  value = yandex_compute_instance.server-vm-1.network_interface.0.nat_ip_address
}

output "internal_ip_server-vm-1" {
  value = yandex_compute_instance.server-vm-1.network_interface.0.ip_address
}