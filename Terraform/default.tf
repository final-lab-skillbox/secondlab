terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

locals {
  folder_id = "b1gtupgkfoh1ec7pk44o"
  cloud_id = "b1gjt5osdte8id75pbg0"
  service_account_file = "/Users/andreyvilkov/authorized_key.json"
}

provider "yandex" {
  service_account_key_file = local.service_account_file
  cloud_id                 = local.cloud_id
  folder_id                = local.folder_id
  zone                     = "ru-central1-b"
}